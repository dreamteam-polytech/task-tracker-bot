package app.dreamteam.task_tracker.bot.tamtam.handlers

import app.dreamteam.task_tracker.bot.core.loggerFind
import app.dreamteam.task_tracker.core.TTrackerException
import chat.tamtam.botapi.TamTamBotAPI
import chat.tamtam.botapi.client.TamTamClient
import chat.tamtam.botapi.model.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch

/**
 * Обработчик событий по взаимодействию пользователей с ботом.
 */
open class TaskTrackerHandler(client: TamTamClient) : TamTamCommunicator, Update.Visitor, CoroutineScope
{
    private val job = SupervisorJob()
    override val coroutineContext = Dispatchers.Default + job

    override val logger = loggerFind()
    override val api = TamTamBotAPI(client)

    override fun visit(model: MessageCreatedUpdate)         = wrap(model) { MessagesHandler.getInstance(api).visit(it) }
    override fun visit(model: MessageCallbackUpdate)        = wrap(model) { MessagesHandler.getInstance(api).visit(it) }
    override fun visit(model: MessageEditedUpdate)          = wrap(model) { MessagesHandler.getInstance(api).visit(it) }
    override fun visit(model: MessageRemovedUpdate)         = wrap(model) { MessagesHandler.getInstance(api).visit(it) }

    override fun visit(model: BotStartedUpdate)             = wrap(model) { BotEventsHandler.getInstance(api).visit(it) }

    override fun visit(model: BotAddedToChatUpdate) {}
    override fun visit(model: BotRemovedFromChatUpdate) {}

    override fun visit(model: UserAddedToChatUpdate) {}
    override fun visit(model: UserRemovedFromChatUpdate) {}

    override fun visit(model: ChatTitleChangedUpdate) {}

    override fun visit(model: MessageConstructionRequest)   = wrap(model) { ConstructorsHandler.getInstance(api).visit(it) }
    override fun visit(model: MessageConstructedUpdate)     = wrap(model) { ConstructorsHandler.getInstance(api).visit(it) }

    override fun visit(model: MessageChatCreatedUpdate) {}

    override fun visitDefault(model: Update) {}
}

/**
 * Безопасно вызывает обработчик запроса для [Update].
 */
fun <T : Update> TaskTrackerHandler.wrap(update: T, action: suspend (T) -> Unit) {
    launch {
        try {
            action.invoke(update)
        } catch (t: Throwable) {
            logger.warn("Exception handled: ${t.message}. " +
                    "Cause: ${t.cause?.message?.substringBefore('\n')}")

            val chatId = when (update) {
                is MessageCreatedUpdate -> update.message.recipient.chatId
                is MessageEditedUpdate -> update.message.recipient.chatId
                is MessageRemovedUpdate -> update.chatId
                is MessageChatCreatedUpdate -> update.chat.chatId
                is BotStartedUpdate -> update.chatId
                else -> null
            }

            chatId?.let {
                val error = "An error occurred:\n${t.localizedMessage}"
                val answer = NewMessageBody(error, null, null)
                api.sendMessage(answer).chatId(it).sendSafely()
            }
        }
    }
}

/**
 * Безопасно вызывает обработчик запроса для [MessageCallbackUpdate].
 * В случае ошибки, отображает её текст пользователю.
 */
fun TaskTrackerHandler.wrap(update: MessageCallbackUpdate, action: suspend (MessageCallbackUpdate) -> Unit) {
    launch {
        try {
            action.invoke(update)
        } catch (t: TTrackerException) {
            logger.warn("Exception handled: ${t.message}. " +
                    "Cause: ${t.cause?.message?.substringBefore('\n')}")

            val answer = CallbackAnswer().apply { notification = t.message }
            api.answerOnCallback(answer, update.callback.callbackId).sendSafely()
        }
    }
}