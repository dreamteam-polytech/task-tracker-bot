package app.dreamteam.task_tracker.bot.tamtam.communications.commands

import app.dreamteam.task_tracker.bot.core.TTrackerCommandException
import app.dreamteam.task_tracker.bot.core.loggerFind
import chat.tamtam.botapi.model.NewMessageBody
import chat.tamtam.botapi.model.User
import com.squareup.moshi.JsonClass

interface CommandParser {
    /**
     * Выполняет обработку распрашенной ранее команды.
     * Выбрасывает исключение, если при исполнении команды произошла ошибка.
     *
     * @param command команда на выполнение
     * @param user пользователь, который запрашивает выполнение команды
     */
    @Throws(TTrackerCommandException::class)
    fun parseCommand(command: Command, user: User): Executor

    /**
     * Выполняет обработку команды в текстовом виде.
     * Выбрасывает исключение, если при парсинге или исполнении команды произошла ошибка.
     *
     * @param command команда на выполнение
     * @param user пользователь, который запрашивает выполнение команды
     */
    @Throws(TTrackerCommandException::class)
    fun parseCommand(command: String?, user: User): Executor

    @JsonClass(generateAdapter = true)
    data class Command(
        val name: String,
        val params: String
    ) {
        val prefix get() = name.substringBefore('@')
        val postfix get() = name.substringAfter('@')
    }

    abstract class Executor protected constructor() {
        protected val logger = loggerFind()

        lateinit var command: Command; private set
        protected lateinit var user: User

        open fun init(command: Command, user: User): Executor {
            this.command = command
            this.user = user
            return this
        }

        /**
         * Выполняет команду.
         *
         * @param page номер страницы (для команд, использующих постраничный вывод)
         * @return тело сообщения, построенное командой
         */
        @Throws(TTrackerCommandException::class)
        abstract suspend fun execute(page: Int = 1): NewMessageBody
    }
}