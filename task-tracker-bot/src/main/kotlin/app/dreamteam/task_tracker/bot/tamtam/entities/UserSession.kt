package app.dreamteam.task_tracker.bot.tamtam.entities

import app.dreamteam.task_tracker.bot.tamtam.communications.tasks.TaskActionInConstructor
import app.dreamteam.task_tracker.core.entities.Task

/**
 * Описывает состояние конструктора сообщений, с которым работает пользователь.
 *
 * @property task текущая конструируемая задача
 * @property currentAction тип действия по изменению задачи, которое запросил пользователь
 */
data class ConstructorState(
    val task: Task,
    val currentAction: TaskActionInConstructor? = null
)

/**
 * Описывает сеанс взаимодействия с пользователем.
 * Хранит полезные служебные данные.
 *
 * @property key уникальный идентификатор сеанса взаимодействия
 * @property userId id пользователя
 * @property activeConstructor текущий активный конструктор задачи
 */
data class UserSession(
    val key: Key,
    val userId: Long? = null,
    val activeConstructor: ConstructorState? = null
) {
    data class Key(
        val sessionId: String
    )
}