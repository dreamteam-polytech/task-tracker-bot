package app.dreamteam.task_tracker.bot.tamtam.handlers

import chat.tamtam.botapi.TamTamBotAPI
import chat.tamtam.botapi.exceptions.APIException
import chat.tamtam.botapi.exceptions.ClientException
import chat.tamtam.botapi.queries.TamTamQuery
import org.slf4j.Logger

/**
 * Интерфейс взаимодействия с API ТамТам.
 */
interface TamTamCommunicator {
    val api: TamTamBotAPI
    val logger: Logger

    suspend fun <T> TamTamQuery<T>.sendSafely(successAction: (suspend (T) -> Unit)? = null) : T? {
        try {
            val result = this.execute()
            successAction?.invoke(result)
            return result
        } catch (e: APIException) {
            logger.error("Failed to execute query {}", this, e)
        } catch (e: ClientException) {
            logger.error("Failed to execute query {}", this, e)
        }
        return null
    }
}