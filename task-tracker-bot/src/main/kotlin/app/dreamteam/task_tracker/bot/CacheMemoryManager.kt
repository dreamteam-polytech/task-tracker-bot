package app.dreamteam.task_tracker.bot

import app.dreamteam.task_tracker.bot.core.loggerFind
import kotlinx.coroutines.*
import kotlinx.coroutines.sync.Mutex
import kotlin.math.max

object CacheMemoryManager : CoroutineScope {
    private val logger = loggerFind()

    private val rootJob = Job()
    override val coroutineContext = Dispatchers.Default + rootJob

    private var cleanJob: Job? = null
    private val mutex = Mutex()
    private val cachesCleanTasks = HashMap<Mutex, LinkedHashMap<*, *>>()

    fun scheduleAutoCleanCaches() {
        if (cleanJob != null && !cleanJob!!.isCancelled) return
        else cleanJob = launch {
            while (true) {
                delay(Main.cacheTimeMins * 60 * 1000L)
                cleanCaches()
            }
        }
    }

    fun register(mapLock: Mutex, map: LinkedHashMap<*, *>) = launch {
        mutex.lock()
        cachesCleanTasks[mapLock] = map
        mutex.unlock()
    }

    fun unregister(mapLock: Mutex) = runBlocking {
        launch {
            mutex.lock()
            cachesCleanTasks.remove(mapLock)
            mutex.unlock()
        }.join()
    }

    private suspend fun cleanCaches() {
        mutex.lock()
        var totalRepos = 0
        var totalElements = 0
        cachesCleanTasks.forEach {
            coroutineScope {
                launch {
                    val mutex = it.key
                    val cache = it.value

                    mutex.lock()
                    val cacheSize = cache.size
                    val removeSize = max(0, cacheSize - Main.cacheRecordsLimit)
                    if (removeSize > 0) {
                        cache.keys.removeAll(
                            listOf(cache.keys.toTypedArray()).subList(0, cacheSize - removeSize)
                        )
                        totalRepos += 1
                        totalElements += removeSize
                    }
                    mutex.unlock()
                }
            }
        }
        if (totalElements > 0)
            logger.info("Cache clean task: removed $totalElements elements from $totalRepos cache repositories")
        mutex.unlock()
    }
}