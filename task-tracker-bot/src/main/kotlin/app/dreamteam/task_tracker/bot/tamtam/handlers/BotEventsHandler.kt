package app.dreamteam.task_tracker.bot.tamtam.handlers

import app.dreamteam.task_tracker.bot.core.loggerFind
import app.dreamteam.task_tracker.bot.core.repositories.ContactRepository
import app.dreamteam.task_tracker.core.TTrackerNetworkException
import app.dreamteam.task_tracker.core.entities.Contact
import chat.tamtam.botapi.TamTamBotAPI
import chat.tamtam.botapi.model.BotStartedUpdate
import chat.tamtam.botapi.model.NewMessageBody

/**
 * Обрабатывает варианты взаимодействия по работе с сообщениями.
 */
class BotEventsHandler private constructor(
    override val api: TamTamBotAPI
) : TamTamCommunicator
{
    companion object {
        @Volatile private var INSTANCE: BotEventsHandler? = null

        fun getInstance(api: TamTamBotAPI): BotEventsHandler =
            INSTANCE ?: synchronized(this) {
                INSTANCE ?: BotEventsHandler(api).also { INSTANCE = it }
            }
    }

    override val logger = loggerFind()

    /**
     * Вызывается, когда пользователь начинает
     * взаимодействие с ботом по кнопке "начать".
     *
     * Сохраняет контакт, который связался с ботом, и ID чата,
     * чтобы иметь возможность связываться с ним.
     */
    suspend fun visit(model: BotStartedUpdate) {
        try {
            val contact = Contact(model.user.name, model.user.userId, model.chatId)
            ContactRepository.save(contact)

            val messageBody = NewMessageBody("Hey! Now you can create and receive tasks!", null, null)
            api.sendMessage(messageBody).chatId(model.chatId).sendSafely()
        } catch (t: TTrackerNetworkException) {
            throw TTrackerNetworkException("""Service is currently unavailable ;(
                |Important:
                |To work correctly, stop me and start again later.""".trimMargin(), t.cause)
        }
    }
}