# TamTam Task Tracker - Bot

## Workspace preparation guide

Open CMD and execute this commands:
1. `git clone git@gitlab.com:dreamteam-polytech/task-tracker-bot.git`
2. `cd task-tracker-bot`
3. `git submodule init`
4. `git submodule update`

After that you need to create your own branch, write some perfect code, and start merge request.