plugins {
    id("org.jetbrains.kotlin.jvm") version  "1.4.31"
    id("com.google.protobuf") version "0.8.16"
    id("com.github.johnrengelman.shadow") version "6.1.0"
}

allprojects {
    group = "app.dreamteam.task_tracker"
    version = "1.0-SNAPSHOT"

    repositories {
        mavenCentral()
        jcenter()
        google()
    }
}

subprojects {
    apply(plugin = "java")

    configure<JavaPluginConvention> {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    apply(plugin = "kotlin")

    tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
        kotlinOptions.jvmTarget = "1.8"
    }
}
