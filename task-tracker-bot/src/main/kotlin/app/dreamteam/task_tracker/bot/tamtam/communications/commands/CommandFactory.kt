package app.dreamteam.task_tracker.bot.tamtam.communications.commands

import app.dreamteam.task_tracker.bot.core.TTrackerInvalidCommandException
import app.dreamteam.task_tracker.bot.core.TTrackerNotACommandException
import chat.tamtam.botapi.model.User

object CommandFactory: CommandParser {
    override fun parseCommand(command: CommandParser.Command, user: User): CommandParser.Executor {
        return when (command.prefix) {
            "show" -> CommandShow().init(command, user)
            else -> throw TTrackerInvalidCommandException("Command not recognized")
        }
    }

    override fun parseCommand(command: String?, user: User): CommandParser.Executor {
        val parsed = parse(command)
        return parseCommand(parsed, user)
    }

    private fun parse(text: String?): CommandParser.Command {
        if (text == null)
            throw TTrackerNotACommandException()

        val trimmed = text.substringAfterLast("/", "").trim()

        val command = trimmed.substringBefore(" ")
        val params = trimmed.substringAfter(" ", "")
        return CommandParser.Command(command, params)
    }
}