package app.dreamteam.task_tracker.bot.core

import app.dreamteam.task_tracker.bot.Main
import app.dreamteam.task_tracker.core.TTrackerException
import app.dreamteam.task_tracker.core.TTrackerMapperException
import app.dreamteam.task_tracker.core.operations.OperationResult
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.util.*

typealias APIException = io.grpc.StatusException
typealias APIStatus = io.grpc.Status

internal inline fun <reified T:Any> T.loggerFind(): Logger = LoggerFactory.getLogger(javaClass.canonicalName)

internal fun Int.zeroIfNegative(): Int = if (this < 0) 0 else this
internal fun String.toUUID(): UUID = UUID.fromString(this)

/** Обращается к сервису используя IO-диспатчер.
 * Отлавливает ошибки при маппинге. */
internal suspend fun <T> service(action: suspend TaskTrackerClient.() -> OperationResult<T>): OperationResult<T> {
    return withContext(Dispatchers.IO) {
        try {
            action(Main.service)
        } catch (t: TTrackerMapperException) {
            OperationResult.Failure(t)
        }
    }
}

open class TTrackerCommandException(message: String, cause: Throwable? = null) : TTrackerException(message, cause)
open class TTrackerInvalidCommandException(message: String = "Unknown command", cause: Throwable? = null) : TTrackerCommandException(message, cause)
open class TTrackerNotACommandException(message: String = "Not a command", cause: Throwable? = null) : TTrackerCommandException(message, cause)

open class TTrackerMessageManagerException(message: String, cause: Throwable? = null) : TTrackerException(message, cause)
