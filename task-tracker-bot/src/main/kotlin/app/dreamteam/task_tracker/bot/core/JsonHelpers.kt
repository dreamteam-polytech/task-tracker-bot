package app.dreamteam.task_tracker.bot.core

import app.dreamteam.task_tracker.bot.tamtam.communications.Payload
import app.dreamteam.task_tracker.bot.tamtam.communications.task_list.TaskListAction
import app.dreamteam.task_tracker.bot.tamtam.communications.tasks.TaskAction
import com.squareup.moshi.FromJson
import com.squareup.moshi.Moshi
import com.squareup.moshi.ToJson
import com.squareup.moshi.adapters.PolymorphicJsonAdapterFactory
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import java.util.*
import kotlin.reflect.KClass

/**
 * Управляет сущностями json-сериализатора Moshi [Moshi].
 * Хранит адаптеры и фабрики для преобразования типов.
 */
internal class JsonHelpers
{
    companion object {
        val moshiInstance: Moshi by lazy {
            val builder = Moshi.Builder()
            builder.add(PlatformJsonHelpers.UUIDJsonAdapter)

            builder.add(ProjectJsonHelpers.payloadJsonAdapterFactory)

            builder.add(ProjectJsonHelpers.taskActionJsonAdapterFactory)
            builder.add(ProjectJsonHelpers.taskActionEditJsonAdapterFactory)
            builder.add(ProjectJsonHelpers.taskActionNavigateJsonAdapterFactory)

            builder.add(ProjectJsonHelpers.taskListActionJsonAdapterFactory)

            builder.add(KotlinJsonAdapterFactory())
            builder.build()
        }
    }

    class PlatformJsonHelpers
    {
        object UUIDJsonAdapter {
            @FromJson fun fromJson(string: String) = UUID.fromString(string)
            @ToJson fun toJson(value: UUID) = value.toString()
        }
    }

    class ProjectJsonHelpers
    {
        companion object {
            val payloadJsonAdapterFactory = generatePolymorphicSealedFactory<Payload>()

            val taskActionJsonAdapterFactory = generatePolymorphicSealedFactory<TaskAction>()
            val taskActionEditJsonAdapterFactory = generatePolymorphicSealedFactory<TaskAction.EditTask>()
            val taskActionNavigateJsonAdapterFactory = generatePolymorphicSealedFactory<TaskAction.Navigate>()

            val taskListActionJsonAdapterFactory = generatePolymorphicSealedFactory<TaskListAction>()

            /**
             * Позволяет сгенерировать фабрику для заданного sealed-класса [T],
             * пригодную для осуществления корректной полиморфной (де-)сериализации подклассов [T].
             */
            private inline fun <reified T : Any> generatePolymorphicSealedFactory(): PolymorphicJsonAdapterFactory<T> {
                return PolymorphicJsonAdapterFactory.of(T::class.java, "class").let {
                    var result = it
                    collectAllSealedSubclasses(T::class).forEach { clazz ->
                        result = result.withSubtype(clazz.java, clazz.qualifiedName)
                    }
                    result
                }!!
            }

            private fun <T : Any> collectAllSealedSubclasses(base: KClass<T>): List<KClass<out T>> {
                val subclasses = mutableListOf<KClass<out T>>()
                base.sealedSubclasses.forEach {
                    subclasses.add(it)
                    subclasses.addAll(collectAllSealedSubclasses(it))
                }
                return subclasses
            }
        }
    }
}