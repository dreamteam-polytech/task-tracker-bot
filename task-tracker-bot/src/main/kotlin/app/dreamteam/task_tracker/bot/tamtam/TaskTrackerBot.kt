package app.dreamteam.task_tracker.bot.tamtam

import app.dreamteam.task_tracker.bot.core.loggerFind
import app.dreamteam.task_tracker.bot.tamtam.handlers.TaskTrackerHandler
import chat.tamtam.bot.longpolling.LongPollingBot
import chat.tamtam.botapi.model.Update

class TaskTrackerBot(accessToken: String) : LongPollingBot(accessToken)
{
    private val logger = loggerFind()
    private val handler by lazy { TaskTrackerHandler(client) }

    override fun onUpdate(update: Update): Any? {
        logger.info("Received update: {}", update.toString().replace(Regex("""[\n\r]+"""), " "))
        update.visit(handler)
        return null
    }

    override fun start() {
        super.start()
        logger.info("Bot started")
    }

    override fun stop() {
        super.stop()
        logger.info("Bot stopped")
    }
}