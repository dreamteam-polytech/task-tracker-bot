package app.dreamteam.task_tracker.bot.tamtam.communications

import app.dreamteam.task_tracker.bot.core.JsonHelpers
import app.dreamteam.task_tracker.bot.tamtam.communications.task_list.TaskListAction
import app.dreamteam.task_tracker.bot.tamtam.communications.tasks.TaskAction
import app.dreamteam.task_tracker.bot.tamtam.communications.tasks.TaskActionInConstructor
import com.squareup.moshi.JsonClass
import java.util.*

/**
 * Класс служит базой для закладывания полезной нагрузки в сообщения.
 * Его потомки могут быть использованы для идентификации конкретного намерения.
 *
 * Пример:
 * Хотим создать сообщение и положить рядом с ним несколько кнопок -> окей, положили, создали.
 * Как определить, когда к нам придет событие клика по кнопке, какая именно кнопка была нажата?
 * Именно в этих целях, изначально при создании сообщения с кнопками, в каждую кнопку необходимо
 * заложить полезную нагрузку, сама сущность которой и будет говорить о её типе.
 */
sealed class Payload
{
    companion object {
        fun fromJson(json: String): Payload {
            val moshi = JsonHelpers.moshiInstance.adapter(Payload::class.java)
            return moshi.fromJson(json)!!
        }
    }

    fun toJson(): String {
        val moshi = JsonHelpers.moshiInstance.adapter(Payload::class.java)
        return moshi.toJson(this)
    }

    /**
     * Пустая полезная нагрузка.
     * Равнозначно "не предпринимать действий".
     */
    @JsonClass(generateAdapter = true)
    class Empty : Payload()

    /**
     * Данные о событии клика по кнопке в сформированной задаче.
     *
     * @property taskUUID уникальный идентификатор задачи
     * @property action намерение, заданное при клике на кнопку
     */
    @JsonClass(generateAdapter = true)
    data class TaskButton(
        val taskUUID: UUID,
        val action: TaskAction
    ) : Payload()

    /**
     * Данные о событии клика по кнопке в конструкторе задачи.
     *
     * @property action тип действия при клике на кнопку
     */
    @JsonClass(generateAdapter = true)
    data class ConstructorButton(
        val action: Action,
        val taskModifyType: TaskActionInConstructor? = null // ToDo: change same as [TaskAction]
    ) : Payload() {
        enum class Action { CHANGE_TASK, GO_BACK }
    }

    /**
     * Данные о событии клика по кнопке в выборке задач.
     */
    @JsonClass(generateAdapter = true)
    data class TaskListButton(
        val action: TaskListAction
    ) : Payload()
}