package app.dreamteam.task_tracker.bot.tamtam

import app.dreamteam.task_tracker.bot.core.repositories.TaskRepository
import app.dreamteam.task_tracker.bot.tamtam.communications.tasks.TaskMessageBuilder
import app.dreamteam.task_tracker.core.TTrackerNetworkException
import app.dreamteam.task_tracker.core.entities.Task
import chat.tamtam.botapi.model.NewMessageBody

/**
 * Сохраняет задачу в репозитории.
 * В случае неудачи вернет тело сообщения с ошибкой, также содержащее описание задачи.
 *
 * @param task задача для сохранения
 * @return null если сохранение успешно, или тело сообщения с ошибкой
 */
suspend fun TaskRepository.saveWithFeedback(task: Task): NewMessageBody? {
    return try {
        save(task)
        null
    } catch (t: TTrackerNetworkException) {
        val taskMessageBody = TaskMessageBuilder(task).buildShort()
        val errorText = """*Failed to save this task to the database*
                |The service may be unavailable. Please try again later""".trimMargin()

        val text = taskMessageBody.text?.let { "$errorText\n---\n$it" } ?: errorText
        NewMessageBody(text, null, null)
    }
}