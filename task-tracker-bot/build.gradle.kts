plugins {
    kotlin("kapt")
}

kapt {
    generateStubs = true
}

sourceSets {
    getByName("main").java.srcDirs("src/main/kotlin")
    getByName("test").java.srcDirs("src/test/kotlin")
}

val grpcVersion = "1.36.0"

dependencies {
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    implementation("com.google.api.grpc:proto-google-common-protos:2.1.0")
    implementation("io.grpc:grpc-netty:${grpcVersion}")
    implementation("io.grpc:grpc-protobuf:${grpcVersion}")
    implementation("io.grpc:grpc-stub:${grpcVersion}")
    implementation("io.grpc:grpc-kotlin-stub:1.0.0")
    implementation("org.slf4j:slf4j-simple:1.7.30")
    implementation(group = "org.jetbrains.kotlinx", name = "kotlinx-coroutines-core", version = "1.4.3")
    implementation(group = "org.jetbrains.kotlinx", name = "kotlinx-coroutines-guava", version = "1.4.3")

    implementation(project(":task-tracker-api"))

    kapt("com.squareup.moshi:moshi-kotlin-codegen:1.11.0")
    kaptTest("com.squareup.moshi:moshi-kotlin-codegen:1.11.0")
    implementation("com.squareup.moshi:moshi:1.11.0")
    implementation("com.squareup.moshi:moshi-kotlin:1.11.0")
    implementation("com.squareup.moshi:moshi-adapters:1.11.0")

    implementation(group = "chat.tamtam", name = "tamtam-bot-sdk", version = "0.0.1")
    implementation(group = "chat.tamtam", name = "tamtam-bot-api", version = "0.3.0")

    testImplementation("io.grpc:grpc-testing:${grpcVersion}")
    testImplementation(group = "junit", name = "junit", version = "4.13.2")
    testImplementation(group = "org.mockito", name = "mockito-core", version = "3.8.0")
}

/** Creates fat Jar. */
val jar by tasks.getting(Jar::class) {
    archiveFileName.set(archiveBaseName.get() + ".jar")

    manifest {
        attributes("Main-Class" to "app.dreamteam.task_tracker.bot.Main")
    }
    from({
        configurations.runtimeClasspath.get().map {
            if (it.isDirectory) it else zipTree(it)
        }
    })
    exclude("META-INF/*.RSA", "META-INF/*.SF", "META-INF/*.DSA")
}