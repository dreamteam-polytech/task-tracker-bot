package app.dreamteam.task_tracker.bot.tamtam.communications.task_list

import app.dreamteam.task_tracker.bot.tamtam.communications.Payload
import app.dreamteam.task_tracker.bot.tamtam.communications.commands.CommandParser
import app.dreamteam.task_tracker.core.entities.Task
import chat.tamtam.bot.builders.attachments.InlineKeyboardBuilder
import chat.tamtam.botapi.model.*
import kotlin.math.min

/**
 * Строит тело сообщения [NewMessageBody] для выборки из нескольких задач [Task].
 * В сообщение добавляются кнопки навигации по страницам.
 *
 * @property pageNumber номер текущей страницы, на которой размещены [tasks].
 * @property totalPages общее количество страниц
 * @property tasks список задач на странице
 * @property command команда, которая выполняется, когда осуществляется переход на другую страницу
 */
class TaskListMessageBuilder(
    private val pageNumber: Int,
    private val totalPages: Int,
    private val tasks: List<Task>,
    private val command: CommandParser.Command
) {
    private lateinit var buttons: InlineKeyboardAttachmentRequest

    private var message: String = "Selected tasks are shown"
    private var noTasksMessage: String = "No tasks to display"

    fun setMessage(mes: String): TaskListMessageBuilder {
        return this.apply { message = mes }
    }

    fun setNoTasksMessage(mes: String): TaskListMessageBuilder {
        return this.apply { noTasksMessage = mes }
    }

    fun build(): NewMessageBody {
        if (totalPages < 1) {
            return NewMessageBody(noTasksMessage, null, null)
        }

        setKeyboardTasksList()
        val attachments = listOf(buttons)
        return NewMessageBody(message, attachments, null)
    }

    /**
     * Устанавливает набор кнопок со списком задач.
     * Добавляет кнопки для перехода по страницам.
     */
    private fun setKeyboardTasksList(): TaskListMessageBuilder {
        var builder = InlineKeyboardBuilder.empty()

        for (task in tasks) {
            //val shortDescription = task.description?.let { if (it.length > 50) it.substring(0, 50) + "..." else it }
            val buttonName = task.status.title + " | " + task.title //+ (shortDescription?.let { " || $it" } ?: "")

            val taskPageButtonAction = TaskListAction.OpenTask(buttonName, task.uuid)
            val taskPageButtonPayload = Payload.TaskListButton(taskPageButtonAction)
            val taskPageButton = CallbackButton(taskPageButtonPayload.toJson(), taskPageButtonAction.text)
            taskPageButton.intent = Intent.POSITIVE
            builder = builder.addRow(taskPageButton)
        }

        val pagination = buildKeyboardPagination()
        if (pagination.isNotEmpty()) {
            builder = builder.addRow(*pagination)
        }

        builder.build().also { this.buttons = it }
        return this
    }

    /**
     * Строит массив кнопок для перехода по страницам.
     * Навигатор по страницам выглядят так: 1 .. 4 5 6 .. 10
     *
     * @return массив кнопок для перехода по страницам
     */
    private fun buildKeyboardPagination(): Array<Button> {
        val result = mutableListOf<Button>()

        val totalButtons = 7 // Нечетное и >= 7!
        val padding = 2

        val pageNumbers: List<Int> = if (totalPages <= totalButtons) {
            val pages = mutableListOf<Int>()
            for (i in 1..totalPages) pages.add(i)
            pages
        } else {
            val padLeft = pageNumber - 1
            val padRight = totalPages - pageNumber

            val pageNumbers = calcPaginationAdjacentValues(pageNumber, totalPages, totalButtons, padding).toMutableList()
            if (padLeft > padding) { // Установит номер страницы == -1 для левого троеточия.
                pageNumbers.removeFirst()
                pageNumbers.add(0, 1)
                pageNumbers.add(1, -1)
            }
            if (padRight > padding) { // Установит номер страницы == -2 для правого троеточия.
                pageNumbers.removeLast()
                pageNumbers.add(totalPages)
                pageNumbers.add(pageNumbers.size - 1, -2)
            }
            pageNumbers
        }

        if (pageNumbers.size > 1) {
            pageNumbers.forEachIndexed { index, page ->
                val pageNumberStr = if (page < 0) "..." else "$page"
                val validPageNumber = when (page) {
                    -1 -> pageNumbers[index + 1] - 1
                    -2 -> pageNumbers[index - 1] + 1
                    else -> page
                }

//                // No action on click same page.
//                val pageButton = if (validPageNumber == pageNumber) {
//                    val pageButtonPayload = Payload.Empty()
//                    CallbackButton(pageButtonPayload.toJson(), pageNumberStr)
//                }
                val pageButtonAction = TaskListAction.Navigate(validPageNumber, command)
                val pageButtonPayload = Payload.TaskListButton(pageButtonAction)
                val pageButton = CallbackButton(pageButtonPayload.toJson(), pageNumberStr)

                pageButton.intent = if (page == pageNumber) Intent.NEGATIVE else Intent.DEFAULT
                result.add(pageButton)
            }
        }

        return result.toTypedArray()
    }

    /**
     * Вычисляет смежные значения номеров страниц для данной.
     */
    private fun calcPaginationAdjacentValues(
        pageNumber: Int, totalPages: Int, inRow: Int, padding: Int
    ) : List<Int> {
        val result = mutableListOf<Int>()

        val align = inRow - padding
        val k = min(totalPages, pageNumber + align / padding)
        for (i in 0 until align) {
            val a = (k - align + i) % align
            val c = k - a
            result.add(if (c < 0) align + c else c)
        }

        return result.sorted()
    }
}