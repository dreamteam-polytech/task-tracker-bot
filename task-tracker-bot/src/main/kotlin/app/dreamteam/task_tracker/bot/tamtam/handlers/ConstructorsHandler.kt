package app.dreamteam.task_tracker.bot.tamtam.handlers

import app.dreamteam.task_tracker.bot.core.loggerFind
import app.dreamteam.task_tracker.bot.core.repositories.TaskRepository
import app.dreamteam.task_tracker.bot.tamtam.communications.Payload
import app.dreamteam.task_tracker.bot.tamtam.communications.tasks.TaskActionInConstructor
import app.dreamteam.task_tracker.bot.tamtam.communications.tasks.TaskMessageBuilder
import app.dreamteam.task_tracker.bot.tamtam.communications.tasks.TaskMessagesManager
import app.dreamteam.task_tracker.bot.tamtam.entities.ConstructorState
import app.dreamteam.task_tracker.bot.tamtam.entities.UserSession
import app.dreamteam.task_tracker.bot.tamtam.repositories.UserSessionRepository
import app.dreamteam.task_tracker.bot.tamtam.saveWithFeedback
import app.dreamteam.task_tracker.core.TTrackerIllegalStateException
import app.dreamteam.task_tracker.core.entities.Contact
import app.dreamteam.task_tracker.core.entities.Task
import chat.tamtam.botapi.TamTamBotAPI
import chat.tamtam.botapi.model.*

/**
 * Обрабатывает варианты взаимодействия по построению задач в конструкторе.
 */
class ConstructorsHandler private constructor(
    override val api: TamTamBotAPI
) : TamTamCommunicator
{
    companion object {
        @Volatile private var INSTANCE: ConstructorsHandler? = null

        fun getInstance(api: TamTamBotAPI): ConstructorsHandler =
            INSTANCE ?: synchronized(this) {
                INSTANCE ?: ConstructorsHandler(api).also { INSTANCE = it }
            }
    }

    override val logger = loggerFind()
    private val taskMessageManager = TaskMessagesManager.getInstance(api)

    /**
     * Будет вызвано, когда пользователь отправит боту какое-либо сообщение
     * или нажмет на кнопку во время конструирования (кнопки конструктора).
     */
    suspend fun visit(model: MessageConstructionRequest) {
        // Получаем результат предыдущего взаимодействия с пользователем.
        val session = UserSessionRepository.load(UserSession.Key(model.sessionId)).copy(userId = model.user.userId)

        val emptyTask by lazy { Task("", Contact(model.user.name, model.user.userId, null)) }
        var activeConstructor = session.activeConstructor ?: ConstructorState(emptyTask, null)
        var task = activeConstructor.task

        // Тело конструктора задачи, которое будет отображено пользователю.
        val constructor: ConstructorAnswer = when (val input = model.input) {
            /* Пользователь что-то отправил собственноручно. */
            is MessageConstructorInput -> {
                // Получаем состояние конструктора и выполняем действие, исходя из него.
                val currentAction = activeConstructor.currentAction
                task = when (currentAction) {
                    TaskActionInConstructor.CHANGE_TITLE -> changeTitle(task, input)
                    TaskActionInConstructor.CHANGE_DESCRIPTION -> changeDescription(task, input)
                    TaskActionInConstructor.CHANGE_ASSIGNEE -> changeAssignee(task, input)
                    else -> parseFreeInput(task, input)
                }

                if (currentAction == null && task.title == activeConstructor.task.title) {
                    // Если заголовок не указан в только что открытом конструкторе,
                    // сразу перейдем на страницу его ввода.
                    activeConstructor = ConstructorState(task, TaskActionInConstructor.CHANGE_TITLE)
                    constructorPageEnterText(task, activeConstructor.currentAction)
                } else {
                    // Сбрасываем состояние конструктора,
                    // отображаем страницу возможных модификаций.
                    activeConstructor = ConstructorState(task, null)
                    constructorPageMain(task)
                }
            }
            /* Пользователь нажал на предложенную кнопку. */
            is CallbackConstructorInput -> {
                // Сохраняем состояние конструктора основываясь на нажатой пользователем кнопке.
                activeConstructor = when (val payload = Payload.fromJson(input.payload)) {
                    is Payload.ConstructorButton -> when (val action = payload.action) {
                        Payload.ConstructorButton.Action.CHANGE_TASK -> {
                            when (val type = payload.taskModifyType) {
                                in TaskActionInConstructor.values() -> ConstructorState(task, type)
                                else -> throw TTrackerIllegalStateException("Unknown action in payload")
                            }
                        }
                        Payload.ConstructorButton.Action.GO_BACK -> ConstructorState(task, null)
                    }
                    else -> throw TTrackerIllegalStateException("Unknown payload")
                }

                when (activeConstructor.currentAction) {
                    null -> constructorPageMain(task)
                    else -> constructorPageEnterText(task, activeConstructor.currentAction)
                }
            }
            else -> throw TTrackerIllegalStateException("Unknown state")
        }

        UserSessionRepository.save(session.copy(activeConstructor = activeConstructor))
        TaskRepository.save(task, cacheOnly = true)
        api.construct(constructor, model.sessionId).sendSafely()
    }

    /**
     * Будет вызвано, когда сообщение, построенное через конструктор, будет опубликовано в каком-либо чате.
     *
     * Редактирует сообщение с задачей, чтобы отображать его краткую форму, вместо полной.
     */
    suspend fun visit(model: MessageConstructedUpdate) {
        val session = UserSessionRepository.load(UserSession.Key(model.sessionId))

        val creatorUserId = session.userId!!
        val task = session.activeConstructor?.task ?: throw TTrackerIllegalStateException("Message was created, but not found in local storage")

        UserSessionRepository.remove(session.key)

        val taskMessageBody = TaskMessageBuilder(task).buildShort()
        val errorBody = TaskRepository.saveWithFeedback(task)

        if (errorBody != null) {
            api.editMessage(errorBody, model.message.body.mid).sendSafely()
        } else {
            api.editMessage(taskMessageBody, model.message.body.mid).sendSafely()
            taskMessageManager.taskCreated(task, creatorUserId, model.message.body.mid)
        }
    }

    /**
     * Строит превью будущей задачи [task].
     */
    private fun constructTaskPreview(task: Task): ConstructorAnswer {
        val constructor = ConstructorAnswer()
        val messageBody = TaskMessageBuilder(task).buildFull()
        constructor.messages = when {
            task.title.isBlank() -> null
            else -> listOf(messageBody)
        }
        return constructor
    }

    /**
     * Строит основную страницу конструктора задачи [task].
     */
    private fun constructorPageMain(task: Task): ConstructorAnswer {
        val constructor = constructTaskPreview(task)
        constructor.isAllowUserInput = false
        constructor.hint = "You can change the task by giving it a name, description and set an assignee"
        constructor.keyboard = constructor.keyboard ?: let {
            val list = mutableListOf<CallbackButton>()
            TaskActionInConstructor.values().forEach {
                val payload = Payload.ConstructorButton(Payload.ConstructorButton.Action.CHANGE_TASK, it).toJson()
                list.add(CallbackButton(payload, it.title))
            }
            Keyboard(listOf(*(list.map { listOf(it) }).toTypedArray()))
        }
        return constructor
    }

    /**
     * Строит страницу с вводом текста для редактирования полей задачи [task].
     * Описание поля ввода немного отличается, в зависимости от [action].
     */
    private fun constructorPageEnterText(task: Task, action: TaskActionInConstructor?): ConstructorAnswer {
        val constructor = constructTaskPreview(task)
        constructor.isAllowUserInput = true
        constructor.hint = when (action) {
            TaskActionInConstructor.CHANGE_TITLE -> "Enter the title of the task being created"
            TaskActionInConstructor.CHANGE_ASSIGNEE -> "Attach registered contact to the message to set an assignee"
            else -> null
        }
        constructor.placeholder = when (action) {
            TaskActionInConstructor.CHANGE_TITLE -> "Enter title for task"
            TaskActionInConstructor.CHANGE_DESCRIPTION -> "Enter description for task"
            TaskActionInConstructor.CHANGE_ASSIGNEE -> "Attach registered contact to the message"
            else -> "Enter text"
        }
        constructor.keyboard = when (action) {
            TaskActionInConstructor.CHANGE_TITLE -> null
            else -> let {
                val payload = Payload.ConstructorButton(Payload.ConstructorButton.Action.GO_BACK)
                Keyboard(listOf(listOf(CallbackButton(payload.toJson(), "Go back"))))
            }
        }
        return constructor
    }

    /**
     * Ищет первое сообщение среди всех сообщений.
     * Сообщение - ввод пользователя.
     *
     * @return текст сообщения или null, если сообщений нет
     */
    private fun findText(input: MessageConstructorInput): String? = let {
        val text = input.messages?.firstOrNull()?.text
        text.let { if (it.isNullOrBlank()) null else it }
    }

    /**
     * Обрабатывает свободный ввод пользователя при создании задачи.
     * Может преобразовать его в заголовок и описание задачи.
     *
     * @param task текущая задача
     * @param input ввод пользователя
     * @return измененная задача
     */
    private fun parseFreeInput(task: Task, input: MessageConstructorInput): Task {
        val text = findText(input)
        return if (text != null) {
            val split = text.split(Regex("""[\n]+"""), 2)
            val title = split[0]
            val description = split.getOrNull(1)
            task.copy(title = title, description = description)
        } else task
    }

    /**
     * Меняет название задачи в соответствии с вводом пользователя.
     *
     * @param task текущая задача
     * @param input ввод пользователя
     * @return измененная задача
     */
    private fun changeTitle(task: Task, input: MessageConstructorInput): Task {
        return findText(input)?.let { task.copy(title = it) } ?: task
    }

    /**
     * Меняет описание задачи в соответствии с вводом пользователя.
     *
     * @param task текущая задача
     * @param input ввод пользователя
     * @return измененная задача
     */
    private fun changeDescription(task: Task, input: MessageConstructorInput): Task {
        return findText(input)?.let { task.copy(description = it) } ?: task
    }

    /**
     * Меняет ответственного за выполнение задачи в соответствии с вводом пользователя.
     *
     * @param task текущая задача
     * @param input ввод пользователя
     * @return измененная задача
     */
    private fun changeAssignee(task: Task, input: MessageConstructorInput): Task {
        val assignee: Contact? = let {
            val message = input.messages?.find { message -> message.attachments?.find { it is ContactAttachmentRequest } != null }
            val request = message?.attachments?.first() as? ContactAttachmentRequest
            val payload = request?.payload ?: return@let null

            // TODO: this exceptions not shown to user, UI only suspends
            val contactName = payload.name ?: throw TTrackerIllegalStateException("Received contact name is NULL")
            val contactId = payload.contactId ?: throw TTrackerIllegalStateException("Contact is not registered TamTam user")
            return@let Contact(contactName, contactId, null)
        }
        return assignee?.let { task.copy(assignee = it) } ?: task
    }
}