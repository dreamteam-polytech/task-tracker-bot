package app.dreamteam.task_tracker.bot.tamtam.communications.tasks

import app.dreamteam.task_tracker.core.entities.Task
import com.squareup.moshi.JsonClass

/**
 * Действия, доступные для сформированной задачи.
 *
 * @property text название действия, отображаемое пользователю
 */
sealed class TaskAction(val text: String) {
    /** Отобразить подробную информацию о задаче. */
    @JsonClass(generateAdapter = true)
    class ShowDetailedTaskInfo: TaskAction("Task details")

    /** Переместиться по навигационому меню. */
    sealed class Navigate(text: String): TaskAction(text) {
        /** Отобразить главный экран для задачи. */
        @JsonClass(generateAdapter = true)
        class MainPage: Navigate("Back")

        /** Отобразить страницу с редактированием задачи. */
        @JsonClass(generateAdapter = true)
        class EditorPage : Navigate("Change the task")
    }

    /** Редактировать задачу. */
    sealed class EditTask(text: String): TaskAction(text) {
        /** Редактировать статус задачи. */
        @JsonClass(generateAdapter = true)
        class EditStatus(val status: Task.Status): EditTask(status.title)
    }
}