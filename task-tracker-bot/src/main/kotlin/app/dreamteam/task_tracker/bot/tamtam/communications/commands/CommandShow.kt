package app.dreamteam.task_tracker.bot.tamtam.communications.commands

import app.dreamteam.task_tracker.bot.Main
import app.dreamteam.task_tracker.bot.core.TTrackerCommandException
import app.dreamteam.task_tracker.bot.core.TTrackerInvalidCommandException
import app.dreamteam.task_tracker.bot.core.repositories.TaskRepository
import app.dreamteam.task_tracker.bot.tamtam.communications.task_list.TaskListMessageBuilder
import app.dreamteam.task_tracker.core.TTrackerIllegalStateException
import app.dreamteam.task_tracker.core.entities.Contact
import app.dreamteam.task_tracker.core.operations.FindTasks
import app.dreamteam.task_tracker.core.operations.OperationResult
import chat.tamtam.botapi.model.NewMessageBody
import chat.tamtam.botapi.model.User

/**
 * Bot command: /show@<subcommand>
 */
class CommandShow : CommandParser.Executor() {

    override fun init(command: CommandParser.Command, user: User): CommandParser.Executor {
        super.init(command, user)
        return when (command.postfix) {
            "reported" -> Reported().init(command, user)
            "assigned" -> Assigned().init(command, user)
            "all" -> All().init(command, user)
            else -> throw TTrackerInvalidCommandException()
        }
    }

    override suspend fun execute(page: Int): NewMessageBody {
        throw TTrackerIllegalStateException("This should never be called")
    }

    class Reported : CommandParser.Executor() {
        override suspend fun execute(page: Int): NewMessageBody {
            val sender = Contact(user.name, user.userId, null)
            val requestObject = FindTasks.Request.TaskRequest(reporter = sender)
            return defaultFindRequest(
                "All of tasks reported by you",
                page, sender, requestObject
            )
        }
    }

    class Assigned : CommandParser.Executor() {
        override suspend fun execute(page: Int): NewMessageBody {
            val sender = Contact(user.name, user.userId, null)
            val requestObject = FindTasks.Request.TaskRequest(assignee = sender)
            return defaultFindRequest(
                "All of tasks assigned to you",
                page, sender, requestObject
            )
        }
    }

    class All : CommandParser.Executor() {
        override suspend fun execute(page: Int): NewMessageBody {
            val sender = Contact(user.name, user.userId, null)
            val requestObject = FindTasks.Request.TaskRequest()
            return defaultFindRequest(
                "All of tasks reported by you and assigned to you",
                page, sender, requestObject
            )
        }
    }
}

suspend fun CommandParser.Executor.defaultFindRequest(
    message: String,
    page: Int,
    sender: Contact,
    requestObject: FindTasks.Request.TaskRequest
): NewMessageBody {
    val request = FindTasks.Request(page, Main.pageSize, sender, requestObject)
    return when (val findResult = TaskRepository.find(request)) {
        is OperationResult.Failure -> throw TTrackerCommandException(findResult.message, findResult.cause)
        is OperationResult.Success -> {
            TaskListMessageBuilder(
                findResult.value.page,
                findResult.value.pagesTotal,
                findResult.value.tasks,
                command
            ).setMessage(message).build()
        }
    }
}