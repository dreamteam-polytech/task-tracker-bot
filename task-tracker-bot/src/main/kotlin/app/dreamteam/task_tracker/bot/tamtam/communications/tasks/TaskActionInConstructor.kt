package app.dreamteam.task_tracker.bot.tamtam.communications.tasks

/**
 * Типы действий по изменению задачи.
 *
 * @property title отображаемый пользователю тип действия
 */
enum class TaskActionInConstructor(val title: String)
{
    CHANGE_TITLE("Change title"),
    CHANGE_DESCRIPTION("Change description"),
    CHANGE_ASSIGNEE("Change assignee")
}