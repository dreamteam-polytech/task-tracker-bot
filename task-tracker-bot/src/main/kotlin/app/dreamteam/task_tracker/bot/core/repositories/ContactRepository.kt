package app.dreamteam.task_tracker.bot.core.repositories

import app.dreamteam.task_tracker.bot.CacheMemoryManager
import app.dreamteam.task_tracker.bot.core.service
import app.dreamteam.task_tracker.core.TTrackerNetworkException
import app.dreamteam.task_tracker.core.entities.Contact
import app.dreamteam.task_tracker.core.mappers.entities.ContactMapper
import app.dreamteam.task_tracker.core.mappers.operations.FindContactsMapper
import app.dreamteam.task_tracker.core.operations.FindContacts
import app.dreamteam.task_tracker.core.operations.OperationResult
import kotlinx.coroutines.sync.Mutex

object ContactRepository
{
    private val mutex = Mutex()
    private val repository = LinkedHashMap<Long, Contact>()

    private val contactMapper = ContactMapper()
    private val findContactsRequestMapper = FindContactsMapper.RequestMapper()
    private val findContactsResponseMapper = FindContactsMapper.ResponseMapper()

    init {
        CacheMemoryManager.register(mutex, repository)
    }

    @Throws(TTrackerNetworkException::class)
    suspend fun save(contact: Contact) {
        mutex.lock()
        val existingValue = repository[contact.tamtamId]
        mutex.unlock()
        if (existingValue != contact) {
            saveRemote(contact)
            mutex.lock()
            repository[contact.tamtamId] = contact
            mutex.unlock()
        }
    }

    suspend fun find(request: FindContacts.Request): OperationResult<FindContacts.Response> = service {
        val requestMapped = findContactsRequestMapper.mapOut(request)
        when (val result = findContacts(requestMapped)) {
            is OperationResult.Failure -> OperationResult.Failure(result.cause)
            is OperationResult.Success -> OperationResult.Success(findContactsResponseMapper.mapIn(result.value))
        }
    }

    @Throws(TTrackerNetworkException::class)
    private suspend fun saveRemote(contact: Contact) = service {
        val contactMapped = contactMapper.mapOut(contact)
        when (val result = updateOrCreateContact(contactMapped)) {
            is OperationResult.Failure -> throw TTrackerNetworkException("Unable to save the contact", result.cause)
            is OperationResult.Success -> OperationResult.Success(null)
        }
    }
}