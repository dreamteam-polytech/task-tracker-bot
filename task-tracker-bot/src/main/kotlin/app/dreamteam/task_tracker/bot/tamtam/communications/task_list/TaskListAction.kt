package app.dreamteam.task_tracker.bot.tamtam.communications.task_list

import app.dreamteam.task_tracker.bot.tamtam.communications.commands.CommandParser
import com.squareup.moshi.JsonClass
import java.util.*

/**
 * Действия, доступные для выборки задач.
 *
 * @property text название действия, отображаемое пользователю
 */
sealed class TaskListAction(val text: String) {
    /**
     * Переместиться по страницам.
     *
     * @property page номер страницы
     * @property command команда для выполнения
     */
    @JsonClass(generateAdapter = true)
    class Navigate(
        val page: Int,
        val command: CommandParser.Command
    ): TaskListAction(page.toString())

    /**
     * Открыть карточку задачи.
     *
     * @property taskUUID уникальный идентификатор задачи
     */
    @JsonClass(generateAdapter = true)
    class OpenTask(text: String, val taskUUID: UUID): TaskListAction(text)
}