package app.dreamteam.task_tracker.bot.core

import app.dreamteam.task_tracker.api.*
import app.dreamteam.task_tracker.core.operations.OperationResult
import com.google.protobuf.Empty
import com.google.protobuf.StringValue
import io.grpc.ManagedChannel
import java.io.Closeable
import java.util.concurrent.TimeUnit

class TaskTrackerClient(private val channel: ManagedChannel) : Closeable {
    private val logger = loggerFind()
    private val stub = TaskTrackerGrpcKt.TaskTrackerCoroutineStub(channel)

    override fun close() {
        channel.shutdown().awaitTermination(5, TimeUnit.SECONDS)
    }

    suspend fun createTask(task: TaskDTO): OperationResult<Empty> {
        return wrap { stub.createTask(task) }
    }

    suspend fun readTask(id: String): OperationResult<TaskDTO> {
        return wrap { stub.readTask(StringValue.of(id)) }
    }

    suspend fun updateTask(task: TaskDTO): OperationResult<Empty> {
        return wrap { stub.updateTask(task) }
    }

    suspend fun deleteTask(id: String): OperationResult<Empty> {
        return wrap { stub.deleteTask(StringValue.of(id)) }
    }

    suspend fun updateOrCreateTask(task: TaskDTO): OperationResult<Empty> {
        return wrap { stub.updateOrCreateTask(task) }
    }

    suspend fun findTasks(request: FindTasksDTO.Request): OperationResult<FindTasksDTO.Response> {
        return wrap { stub.findTasks(request) }
    }

    suspend fun updateOrCreateContact(contact: ContactDTO): OperationResult<Empty> {
        return wrap { stub.updateOrCreateContact(contact) }
    }

    suspend fun findContacts(request: FindContactsDTO.Request): OperationResult<FindContactsDTO.Response> {
        return wrap { stub.findContacts(request) }
    }

    suspend fun manageTaskMessages(request: ManageTaskMessagesDTO.Request): OperationResult<ManageTaskMessagesDTO.Response> {
        return wrap { stub.manageTaskMessages(request) }
    }

    private inline fun <R> wrap(action: () -> R): OperationResult<R> {
        return try {
            OperationResult.Success(action())
        } catch (t: APIException) {
            OperationResult.Failure(t, "Got status: ${t.status}, cause: ${t.localizedMessage}")
        } catch (t: Throwable) {
            OperationResult.Failure(t)
        }
    }
}