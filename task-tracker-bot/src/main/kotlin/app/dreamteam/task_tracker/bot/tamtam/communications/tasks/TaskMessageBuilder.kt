package app.dreamteam.task_tracker.bot.tamtam.communications.tasks

import app.dreamteam.task_tracker.bot.tamtam.communications.Payload
import app.dreamteam.task_tracker.core.entities.Task
import chat.tamtam.bot.builders.attachments.InlineKeyboardBuilder
import chat.tamtam.botapi.model.CallbackButton
import chat.tamtam.botapi.model.InlineKeyboardAttachmentRequest
import chat.tamtam.botapi.model.Intent
import chat.tamtam.botapi.model.NewMessageBody
import kotlin.math.min

/**
 * Строит тело сообщения [NewMessageBody] для задачи [Task] в установленном формате.
 * Его можно использовать для создания новых и редактирования существующих сообщений в мессенджере.
 *
 * @property task задача, исходя из которой будет построено сообщение
 */
open class TaskMessageBuilder(private val task: Task)
{
    open val title: String get() = "*${task.title.trim()}*"

    open val status: String get() = "Status: " + task.status.title

    open val description: String get() = task.description?.trim() ?: ""

    open val assignee: String get() = "Assignee: " + (task.assignee?.name ?: "No assignee")

    open val reporter: String get() = "Reporter: " + task.reporter.name

    open val shortMessage: String get() {
        return "$title\n$assignee\n$status"
    }

    open val fullMessage: String get() {
        return "$title\n$assignee\n$status\n$reporter" + if (description.isNotBlank()) "\n\n$description" else ""
    }

    open lateinit var buttons: InlineKeyboardAttachmentRequest

    init {
        setKeyboardMain()
    }

    /**
     * Устанавливает основной набор кнопок.
     */
    fun setKeyboardMain(): TaskMessageBuilder {
        var builder = InlineKeyboardBuilder.empty()

        val infoButtonAction = TaskAction.ShowDetailedTaskInfo()
        val infoButtonPayload = Payload.TaskButton(task.uuid, infoButtonAction)
        val infoButton = CallbackButton(infoButtonPayload.toJson(), infoButtonAction.text)

        val editButtonAction = TaskAction.Navigate.EditorPage()
        val editButtonPayload = Payload.TaskButton(task.uuid, editButtonAction)
        val editButton = CallbackButton(editButtonPayload.toJson(), editButtonAction.text)

        builder = builder.addRow(editButton, infoButton)

        builder.build().also { this.buttons = it }
        return this
    }

    /**
     * Устанавливает набор кнопок для смены статуса задачи.
     *
     * @param buttonsInRow количество кнопок в одной линии клавиатуры
     */
    fun setKeyboardEditor(buttonsInRow: Int = 3): TaskMessageBuilder {
        var builder = InlineKeyboardBuilder.empty()
        val buttons = Task.Status.values().filter { it !== task.status }.toTypedArray()

        val backButtonAction = TaskAction.Navigate.MainPage()
        val backButtonPayload = Payload.TaskButton(task.uuid, backButtonAction)
        val backButton = CallbackButton(backButtonPayload.toJson(), backButtonAction.text)
        backButton.intent = Intent.NEGATIVE
        builder = builder.addRow(backButton)

        for (i in buttons.indices step buttonsInRow) {
            val rowTasks = buttons.sliceArray(IntRange(i, min(i + buttonsInRow, buttons.size) - 1))
            val rowButtons = rowTasks.map { status ->
                val action = TaskAction.EditTask.EditStatus(status)
                val payload = Payload.TaskButton(task.uuid, action)
                CallbackButton(payload.toJson(), action.text)
            }
            builder = builder.addRow(*rowButtons.toTypedArray())
        }

        builder.build().also { this.buttons = it }
        return this
    }

    /**
     * Строит краткую карточку задачи.
     *
     * @return задача, пригодная для отправки в виде сообщения
     */
    fun buildShort(): NewMessageBody {
        val attachments = listOf(buttons)
        return NewMessageBody(shortMessage, attachments, null)
    }

    /**
     * Строит информативную карточку задачи.
     *
     * @return задача, пригодная для отправки в виде сообщения
     */
    fun buildFull(): NewMessageBody {
        return NewMessageBody(fullMessage, null, null)
    }
}