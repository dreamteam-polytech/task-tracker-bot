package app.dreamteam.task_tracker.bot.tamtam.communications.tasks

import app.dreamteam.task_tracker.bot.Main
import app.dreamteam.task_tracker.bot.core.TTrackerMessageManagerException
import app.dreamteam.task_tracker.bot.core.loggerFind
import app.dreamteam.task_tracker.bot.core.repositories.ContactRepository
import app.dreamteam.task_tracker.bot.core.repositories.TaskRepository
import app.dreamteam.task_tracker.bot.tamtam.handlers.TamTamCommunicator
import app.dreamteam.task_tracker.core.TTrackerException
import app.dreamteam.task_tracker.core.TTrackerNetworkException
import app.dreamteam.task_tracker.core.entities.Contact
import app.dreamteam.task_tracker.core.entities.Task
import app.dreamteam.task_tracker.core.operations.FindContacts
import app.dreamteam.task_tracker.core.operations.ManageTaskMessages
import app.dreamteam.task_tracker.core.operations.OperationResult
import chat.tamtam.bot.builders.NewMessageBodyBuilder
import chat.tamtam.botapi.TamTamBotAPI
import chat.tamtam.botapi.model.NewMessageBody
import kotlinx.coroutines.*
import java.util.*

/**
 * Менеджер сообщений, содержащих в себе задачи [Task].
 * При очередной отправке сообщения с задачей в мессенджере,
 * связывает его с дальнейшими обновляениями этой задачи.
 *
 * Обрабатывает исходы создания новой задачи (отправка оповещания назначенному лицу),
 * обновления задачи (обновляет задачи в других чатах).
 */
class TaskMessagesManager(
    override val api: TamTamBotAPI
) : TamTamCommunicator, CoroutineScope
{
    companion object {
        @Volatile private var INSTANCE: TaskMessagesManager? = null

        fun getInstance(api: TamTamBotAPI): TaskMessagesManager =
            INSTANCE ?: synchronized(this) {
                INSTANCE ?: TaskMessagesManager(api).also { INSTANCE = it }
            }
    }

    override val logger = loggerFind()
    override val coroutineContext = Dispatchers.Default + SupervisorJob()

    /**
     * Вызывается, когда создается новая задача и сообщение в чате с ней.
     * Подписывает сообщение в мессенджере на изменения в задаче.
     * Уведомляет назначенного на задачу пользователя.
     *
     * @param task созданная задача
     * @param messageId идентификатор сообщения в мессенджере
     */
    suspend fun taskCreated(task: Task, creatorId: Long, messageId: String) = withContext(coroutineContext) {
        findContact(creatorId)?.tamtamChatId?.let { chatId ->
            taskMessageCreated(task.uuid, chatId, messageId)
        } ?: throw TTrackerException("Not found chat id for task creator")

        val assignee = task.assignee ?: return@withContext
        sendTaskToContactAsync(assignee.tamtamId, task).await()
    }

    /**
     * Вызывается перед тем, как сообщение с задачей должно быть отредактировано.
     *
     * Чтобы поддержать одну и ту же задачу актуальной в разных чатах,
     * остальные сообщения с той же задачей также необходимо отредактировать.
     * Этот метод запускает процесс обновления этих сообщений после выполнения [updateAction].
     *
     * Также будет обработано изменение полей задачи.
     *
     * @param oldTask задача до редактирования
     * @param updatedTask задача после редактирования
     * @param updateAction действие по обновлению сообщения, возвращающее ID этого сообщения
     * @return список сообщений, которые также нужно отредактировать
     */
    @Throws(TTrackerMessageManagerException::class)
    suspend fun taskMessageUpdate(oldTask: Task, updatedTask: Task, updateAction: suspend (Task) -> String) = withContext(coroutineContext) {
        parseTaskChanges(oldTask, updatedTask)
        try {
            val request = ManageTaskMessages.Request.GetMessages(updatedTask.uuid)
            val response: ManageTaskMessages.Response.GetMessages = TaskRepository.manageTaskMessages(request)

            val currentMessageId = updateAction(updatedTask)

            val messagesToUpdate = response.messageIds.toMutableList().also { it.remove(currentMessageId) }
            updateMessages(updatedTask, messagesToUpdate)
        } catch (t: TTrackerNetworkException) {
            throw TTrackerMessageManagerException("Failed to update other messages with this task", t.cause)
        }
        return@withContext
    }

    /**
     * Вызывается после того, как будет создано сообщение в мессенджере для данной задачи.
     * Сообщение должно быть связано с задачей для возможности его обновления (например, из других чатов).
     *
     * Данный метод может завершится с ошибкой (исключением).
     * Тогда следует уведомить пользователя о том, что сообщение не будет обновляться.
     *
     * @param taskId уникальный идентификатор задачи, для которой формируется сообщение
     * @param messageId идентификатор сообщения в мессенджере
     */
    // TODO: сделать небольшой кэш, чтобы собирать запросы с одним и тем же taskId и отправлять одним списком
    @Throws(TTrackerMessageManagerException::class)
    suspend fun taskMessageCreated(taskId: UUID, chatId: Long, messageId: String) = withContext(coroutineContext) {
        try {
            val request = ManageTaskMessages.Request.PinMessages(taskId, listOf(chatId to messageId))
            val response: ManageTaskMessages.Response.PinMessages = TaskRepository.manageTaskMessages(request)
            if (Main.deleteUnpinnedTaskMessages) {
                response.unpinnedMessageIds.map { unpinnedMessageId ->
                    launch { api.deleteMessage(unpinnedMessageId).sendSafely() }
                }
            }
        } catch (t: TTrackerNetworkException) {
            throw TTrackerMessageManagerException("""Failed to register the created task.
                |The task message will not receive updates.
                |Try again. To do this, find and open this task.""".trimMargin(), t.cause)
        }
    }

    /**
     * Обрабатывает изменения полей в задаче.
     * Например, если статус задачи установился в завершенный, постановщик задачи будет оповещен о её завершении.
     *
     * @param oldTask задача до изменений
     * @param updatedTask задача после изменений
     */
    private fun parseTaskChanges(oldTask: Task, updatedTask: Task) = launch {
        if (oldTask.status === updatedTask.status) return@launch

        val reporter = updatedTask.reporter
        val assignee = updatedTask.assignee
        val status = updatedTask.status

        if (status === Task.Status.DONE && reporter != assignee && assignee != null) {
            findContact(reporter.tamtamId)?.tamtamChatId?.apply {
                val text = "Task '${updatedTask.title}' has now '${status.title}' status (assignee: ${assignee.name})"
                val body = NewMessageBodyBuilder.ofText(text).build()
                api.sendMessage(body).chatId(this).sendSafely()
            }
        }
    }

    /**
     * Обновляет указанные сообщения [messagesToUpdate],
     * устанавливая в качестве их тела задачу [task].
     *
     * @param task задача, которая будет установлена в сообщения
     * @param messagesToUpdate список сообщений, подлежащих обновлению
     */
    private fun updateMessages(task: Task, messagesToUpdate: List<String>) = launch {
        val messageBody = buildMessageBody(task)
        messagesToUpdate.forEach {
            launch {
                api.editMessage(messageBody, it).sendSafely()
            }
        }
    }

    /**
     * Отправляет задачу пользователю с установленным [tamtamID],
     * если ранее он начинал общение с ботом.
     * Будет выполнен поиск ID чата, чтобы доставить сообщение.
     *
     * @param tamtamID уникальный идентификатор пользователя
     * @param task задача для отправки
     */
    private fun sendTaskToContactAsync(tamtamID: Long, task: Task) = async {
        findContact(tamtamID)?.tamtamChatId?.apply {
            sendTaskToChatAsync(this, task).await()
        }
    }

    /**
     * Отправляет задачу пользователю в чат,
     * если ранее он начинал общение с ботом.
     *
     * @param chatId уникальный идентификатор чата
     * @param task задача для отправки
     */
    private fun sendTaskToChatAsync(chatId: Long, task: Task) = async {
        val messageBody = buildMessageBody(task)
        api.sendMessage(messageBody).chatId(chatId).sendSafely {
            taskMessageCreated(task.uuid, chatId, it.message.body.mid)
        } ?: throw TTrackerException("Message was not sent")
    }

    /**
     * Осуществляет поиск контакта.
     * Если ранее он был зарегистрирован, то возвратится должно ненулевое значение.
     *
     * @param tamtamId уникальный идентификатор пользователя в Там Там
     * @return null, или контакт пользователя с данным [tamtamId]
     */
    private suspend fun findContact(tamtamId: Long): Contact? {
        val body = FindContacts.Request.ContactRequest(tamtamId = tamtamId)
        val request = FindContacts.Request(1, 1, body)
        return when (val response = ContactRepository.find(request)) {
            is OperationResult.Failure -> null
            is OperationResult.Success -> response.value.body.firstOrNull()
        }
    }

    private fun buildMessageBody(task: Task): NewMessageBody {
        return TaskMessageBuilder(task).buildShort()
    }
}