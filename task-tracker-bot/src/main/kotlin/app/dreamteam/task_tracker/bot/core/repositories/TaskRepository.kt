package app.dreamteam.task_tracker.bot.core.repositories

import app.dreamteam.task_tracker.bot.CacheMemoryManager
import app.dreamteam.task_tracker.bot.core.service
import app.dreamteam.task_tracker.core.TTrackerException
import app.dreamteam.task_tracker.core.TTrackerNetworkException
import app.dreamteam.task_tracker.core.entities.Task
import app.dreamteam.task_tracker.core.mappers.entities.TaskMapper
import app.dreamteam.task_tracker.core.mappers.operations.FindTasksMapper
import app.dreamteam.task_tracker.core.mappers.operations.ManageTaskMessagesMapper
import app.dreamteam.task_tracker.core.operations.FindTasks
import app.dreamteam.task_tracker.core.operations.ManageTaskMessages
import app.dreamteam.task_tracker.core.operations.OperationResult
import app.dreamteam.task_tracker.core.operations.unwrap
import kotlinx.coroutines.sync.Mutex
import java.util.*

/**
 * Репозиторий, осуществляющий хранение и поиск задач [Task].
 */
object TaskRepository
{
    private val mutex = Mutex()
    private val repository = LinkedHashMap<UUID, Task>()

    private val taskMapper = TaskMapper()
    private val findTasksRequestMapper = FindTasksMapper.RequestMapper()
    private val findTasksResponseMapper = FindTasksMapper.ResponseMapper()

    private val manageTaskMessagesRequestMapper = ManageTaskMessagesMapper.RequestMapper()
    private val manageTaskMessagesResponseMapper = ManageTaskMessagesMapper.ResponseMapper()

    init {
        CacheMemoryManager.register(mutex, repository)
    }

    /**
     * Сохраняет задачу [task] в память.
     * Если стоит параметр [cacheOnly], тогда задача будеот сохранена
     * только в локальной памяти быстрого доступа.
     *
     * @param task задача для сохранения
     * @param cacheOnly сохранить задачу только в кэш-памяти
     * @throws TTrackerNetworkException если удаленное сохранение прошло неудачно
     */
    @Throws(TTrackerNetworkException::class)
    suspend fun save(task: Task, cacheOnly: Boolean = false) {
        if (!cacheOnly)
            saveRemote(task)
        mutex.lock()
        repository[task.uuid] = task
        mutex.unlock()
    }

    /**
     * Получает задачу из памяти по её id.
     *
     * @param uuid идентификатор задачи
     * @return найденная задача
     * @throws TTrackerNetworkException если удаленное чтение прошло неудачно
     */
    @Throws(TTrackerNetworkException::class)
    suspend fun read(uuid: UUID): Task {
        mutex.lock()
        val value = repository[uuid]
        mutex.unlock()
        return value ?: readRemote(uuid).also { repository[uuid] = it }
    }

    /**
     * Осуществляет поиск задач на удаленном сервере по запросу [request].
     *
     * @param request запрос на поиск
     * @return результат операции
     */
    suspend fun find(request: FindTasks.Request): OperationResult<FindTasks.Response> = service {
        val requestMapped = findTasksRequestMapper.mapOut(request)
        when (val result = findTasks(requestMapped)) {
            is OperationResult.Failure -> OperationResult.Failure(result.cause)
            is OperationResult.Success -> OperationResult.Success(findTasksResponseMapper.mapIn(result.value))
        }
    }

    /**
     * Осуществляет запрос на управление сообщениями, связанными с задачей.
     * Возвращаемое значение должно быть указано явно типом [R].
     *
     * @param request запрос
     * @return ответ типа [R]
     * @throws TTrackerException если операция не смогла выполниться корректно
     */
    @Throws(TTrackerNetworkException::class)
    suspend fun <R : ManageTaskMessages.Response> manageTaskMessages(request: ManageTaskMessages.Request): R = service {
        val requestMapped = manageTaskMessagesRequestMapper.mapOut(request)
        when (val result = manageTaskMessages(requestMapped)) {
            is OperationResult.Failure -> throw TTrackerNetworkException("Unable to finish manage task messages operation", result.cause)
            is OperationResult.Success -> OperationResult.Success(manageTaskMessagesResponseMapper.mapIn(result.value))
        }
    }.unwrap() as R

    @Throws(TTrackerNetworkException::class)
    private suspend fun saveRemote(task: Task) = service {
        val taskMapped = taskMapper.mapOut(task)
        when (val result = updateOrCreateTask(taskMapped)) {
            is OperationResult.Failure -> throw TTrackerNetworkException("Unable to save the task", result.cause)
            is OperationResult.Success -> OperationResult.Success(null)
        }
    }

    @Throws(TTrackerNetworkException::class)
    private suspend fun readRemote(uuid: UUID): Task = service {
        when (val result = readTask(uuid.toString())) {
            is OperationResult.Failure -> throw TTrackerNetworkException("Unable to load the task", result.cause)
            is OperationResult.Success -> OperationResult.Success(taskMapper.mapIn(result.value))
        }
    }.unwrap()!!
}