package app.dreamteam.task_tracker.bot.tamtam.repositories

import app.dreamteam.task_tracker.bot.CacheMemoryManager
import app.dreamteam.task_tracker.bot.tamtam.entities.UserSession
import kotlinx.coroutines.sync.Mutex

/**
 * Репозиторий, кеширующий взаимодействие с пользователем.
 */
object UserSessionRepository
{
    private val mutex = Mutex()
    private val repository = LinkedHashMap<UserSession.Key, UserSession>()

    init {
        CacheMemoryManager.register(mutex, repository)
    }

    suspend fun save(userSession: UserSession) {
        mutex.lock()
        repository[userSession.key] = userSession
        mutex.unlock()
    }

    suspend fun load(key: UserSession.Key): UserSession {
        mutex.lock()
        val value = repository[key] ?: UserSession(key)
        mutex.unlock()
        return value
    }

    suspend fun remove(key: UserSession.Key) {
        mutex.lock()
        repository.remove(key)
        mutex.unlock()
    }
}