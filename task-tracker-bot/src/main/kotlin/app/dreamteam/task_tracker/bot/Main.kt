package app.dreamteam.task_tracker.bot

import app.dreamteam.task_tracker.bot.core.TaskTrackerClient
import app.dreamteam.task_tracker.bot.core.loggerFind
import app.dreamteam.task_tracker.bot.tamtam.TaskTrackerBot
import chat.tamtam.bot.exceptions.TamTamBotException
import chat.tamtam.bot.longpolling.LongPollingBot
import io.grpc.ManagedChannelBuilder
import kotlin.system.exitProcess

object Main {
    /** Размер страницы с задачами. */
    const val pageSize: Int = 7
    /** Удалять сообщения с задачами, которые больше не получают обновления. */
    const val deleteUnpinnedTaskMessages: Boolean = true
    /* Кэш репозиториев. Время самоочистки зарегистрированного кэша в минутах. */
    const val cacheTimeMins: Int = 15
    /* Кэш репозиториев. Количество записей в кэше, которое необходимо оставить после самоочистки. */
    const val cacheRecordsLimit: Int = 10000

    private val logger = loggerFind()
    internal lateinit var service: TaskTrackerClient

    @JvmStatic
    fun main(args: Array<String>) {
        connectToService()
        startBot()

        CacheMemoryManager.scheduleAutoCleanCaches()
    }

    private fun connectToService() = try {
        logger.info("Connecting to service...")

        val ip = System.getenv("SERVICE_IP")
        val port = System.getenv("SERVICE_PORT").toInt()

        val channel = ManagedChannelBuilder.forAddress(ip, port).usePlaintext().build()
        service = TaskTrackerClient(channel)
    } catch (e: Throwable) {
        logger.error("Failed to connect to the service: " + e.message)
        exitProcess(1)
    }

    private fun startBot() = try {
        logger.info("Starting bot...")

        val accessToken = System.getenv("BOT_TOKEN")
        val bot: LongPollingBot = TaskTrackerBot(accessToken)

        Runtime.getRuntime().addShutdownHook(Thread {
            logger.info("Stopping bot...")
            bot.stop()
        })

        bot.start()
    } catch (e: TamTamBotException) {
        logger.error("Failed to start bot: " + e.message)
        exitProcess(1)
    }
}