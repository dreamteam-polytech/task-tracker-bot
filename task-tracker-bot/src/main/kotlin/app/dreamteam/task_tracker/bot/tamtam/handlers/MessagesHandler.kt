package app.dreamteam.task_tracker.bot.tamtam.handlers

import app.dreamteam.task_tracker.bot.core.loggerFind
import app.dreamteam.task_tracker.bot.core.repositories.TaskRepository
import app.dreamteam.task_tracker.bot.tamtam.communications.Payload
import app.dreamteam.task_tracker.bot.tamtam.communications.commands.CommandFactory
import app.dreamteam.task_tracker.bot.tamtam.communications.task_list.TaskListAction
import app.dreamteam.task_tracker.bot.tamtam.communications.tasks.TaskAction
import app.dreamteam.task_tracker.bot.tamtam.communications.tasks.TaskMessageBuilder
import app.dreamteam.task_tracker.bot.tamtam.communications.tasks.TaskMessagesManager
import app.dreamteam.task_tracker.bot.tamtam.saveWithFeedback
import app.dreamteam.task_tracker.core.TTrackerException
import app.dreamteam.task_tracker.core.TTrackerIllegalAccessException
import app.dreamteam.task_tracker.core.entities.Contact
import app.dreamteam.task_tracker.core.entities.Task
import app.dreamteam.task_tracker.core.entities.validateAccess
import chat.tamtam.botapi.TamTamBotAPI
import chat.tamtam.botapi.model.*

/**
 * Обрабатывает варианты взаимодействия по работе с сообщениями.
 */
class MessagesHandler private constructor(
    override val api: TamTamBotAPI
) : TamTamCommunicator
{
    companion object {
        @Volatile private var INSTANCE: MessagesHandler? = null

        fun getInstance(api: TamTamBotAPI): MessagesHandler =
            INSTANCE ?: synchronized(this) {
                INSTANCE ?: MessagesHandler(api).also { INSTANCE = it }
            }
    }

    override val logger = loggerFind()
    private val taskMessageManager = TaskMessagesManager.getInstance(api)

    /**
     * Вызывается, когда создается сообщение.
     */
    suspend fun visit(model: MessageCreatedUpdate) {
        if (model.message.recipient.chatType === ChatType.CHANNEL)
            throw TTrackerException("Task Tracker service available only for users, not for channels")
        //api.getChat(model.message.recipient.chatId).execute().ownerId

        val command = CommandFactory.parseCommand(model.message.body.text, model.message.sender)
        val body = command.execute()
        api.sendMessage(body).chatId(model.message.recipient.chatId).sendSafely()
    }

    /**
     * Вызывается, когда пользователь нажимает на кнопку в сообщении.
     */
    suspend fun visit(model: MessageCallbackUpdate) {
        val message = model.message ?: return // если null, то сообщение было удалено
        when (val payload = Payload.fromJson(model.callback.payload)) {
            is Payload.TaskButton -> {
                val task = TaskRepository.read(payload.taskUUID)
                val user = model.callback.user

                val notifications = mutableListOf<String>()
                val (modifiedTask, messageBody) = when (val action = payload.action) {
                    is TaskAction.EditTask -> {
                        val (newTask, notes) = editTask(user, task, action)
                        notifications.addAll(notes)
                        val builder = TaskMessageBuilder(newTask)
                        newTask to builder.buildShort()
                    }
                    is TaskAction.ShowDetailedTaskInfo -> {
                        val builder = TaskMessageBuilder(task)
                        val showDetailed = showDetailedTaskInfo(message.body.text ?: "", builder)
                        null to if (showDetailed) builder.buildFull() else builder.buildShort()
                    }
                    is TaskAction.Navigate -> {
                        val (builder, notes) = navigate(task, action)
                        notifications.addAll(notes)
                        null to builder.buildShort()
                    }
                }

                val callbackAnswer = CallbackAnswer().apply {
                    this.message = messageBody
                    this.notification = notifications.joinToString("; ")
                }

                if (modifiedTask == null) {
                    api.answerOnCallback(callbackAnswer, model.callback.callbackId).sendSafely()
                    return
                }

                val errorBody = TaskRepository.saveWithFeedback(modifiedTask)
                if (errorBody != null) {
                    val callbackAnswerWithError = CallbackAnswer().message(errorBody)
                    api.answerOnCallback(callbackAnswerWithError, model.callback.callbackId).sendSafely()
                } else {
                    taskMessageManager.taskMessageUpdate(task, modifiedTask) {
                        api.answerOnCallback(callbackAnswer, model.callback.callbackId).sendSafely()
                        return@taskMessageUpdate message.body.mid
                    }
                }
            }
            is Payload.TaskListButton -> {
                when (val action = payload.action) {
                    is TaskListAction.Navigate -> {
                        val command = CommandFactory.parseCommand(action.command, model.callback.user)
                        val messageBody = command.execute(action.page)
                        val callbackAnswer = CallbackAnswer().apply { this.message = messageBody }
                        api.answerOnCallback(callbackAnswer, model.callback.callbackId).sendSafely()
                    }
                    is TaskListAction.OpenTask -> {
                        val task = TaskRepository.read(action.taskUUID)

                        // Check if the owner clicks the open task button.
                        val candidate = Contact(model.callback.user.name, model.callback.user.userId, null)
                        task.validateAccess(candidate)

                        val messageBody = TaskMessageBuilder(task).buildShort()
                        val chatId = message.recipient.chatId!!
                        api.sendMessage(messageBody).chatId(chatId).sendSafely {
                            taskMessageManager.taskMessageCreated(task.uuid, chatId, it.message.body.mid)
                        }
                    }
                }
            }
        }
    }

    private fun navigate(task: Task, navigateAction: TaskAction.Navigate): Pair<TaskMessageBuilder, List<String>> {
        val builder = TaskMessageBuilder(task)
        when (navigateAction) {
            is TaskAction.Navigate.EditorPage -> builder.setKeyboardEditor()
            is TaskAction.Navigate.MainPage -> builder.setKeyboardMain()
        }
        return builder to emptyList()
    }

    /**
     * Изменяет задачу в соответствии с потребностями пользователя.
     * Проверяет полномочия действий для пользователя [user].
     *
     * @param user пользователь, запросивший действие
     * @param task текущая задача
     * @param editAction намерение на изменение задачи
     * @return Pair<измененная задача, список уведомлений пользователю>
     */
    private fun editTask(user: User, task: Task, editAction: TaskAction.EditTask): Pair<Task, List<String>> {
        val reporter = task.reporter
        val assignee = task.assignee

        // Проверка прав редактирования задачи.
        when {
            user.userId == reporter.tamtamId -> {}
            user.userId == assignee?.tamtamId -> {}
            assignee?.tamtamId == null -> {}
            else -> throw TTrackerIllegalAccessException("Only reporter and assignee can change the task")
        }

        val (editedTask, notifications) = when (editAction) {
            is TaskAction.EditTask.EditStatus -> changeStatus(user, task, editAction.status)
        }

        return editedTask to notifications
    }

    /**
     * Определяет, нужно ли показать подробную информацию по задаче.
     *
     * @param message текущий текст сообщения задачи
     * @param taskBuilder конструктор сообщения задачи
     * @return true - нужно отобразить задачу подробно, иначе false
     */
    private fun showDetailedTaskInfo(message: String, taskBuilder: TaskMessageBuilder): Boolean {
        return message.length < taskBuilder.fullMessage.length
    }

    /**
     * Меняет статус выполнения задачи.
     *
     * @param user текущий пользователь
     * @param task текущая задача
     * @param newStatus новый статус для установки
     * @return Pair<измененная задача, список уведомлений пользователю>
     */
    private fun changeStatus(user: User, task: Task, newStatus: Task.Status): Pair<Task, List<String>> {
        if (task.status === newStatus) return Pair(task, emptyList())

        val notifications = mutableListOf<String>()
        val newAssignee = when (task.assignee?.tamtamId) {
            null -> {
                notifications.add("This task has been assigned to you")
                Contact(user.name, user.userId, null)
            }
            else -> task.assignee
        }

        val newTask = task.copy(status = newStatus, assignee = newAssignee)
        notifications.add("Task status has been changed to '" + newStatus.title + "'")

        return newTask to notifications
    }

    /**
     * Вызывается, когда сообщение редактируется.
     */
    suspend fun visit(model: MessageEditedUpdate) {
        // TODO("Not yet implemented")
//        val body = NewMessageBody("Editing not supported yet", null, null)
//        api.sendMessage(body).chatId(model.message.recipient.chatId).sendSafely()
    }

    /**
     * Вызывается, когда сообщение удаляется.
     */
    suspend fun visit(model: MessageRemovedUpdate) {
        // TODO("Not yet implemented")
    }
}